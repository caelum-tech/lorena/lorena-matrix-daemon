#!/usr/bin/env node
const { Bot } = require('./src/bot.js')
const config = require('./src/config.js')
const fetch = require('node-fetch')
const JSZip = require('jszip')
const { createClient } = require('matrix-js-sdk')
const { Server } = require('./src/server.js')
const { Store } = require('./src/store.js')

let bot, server, store, publicLobby
const authenticationRooms = {}

/**
 * Start server
 *
 * @returns {Server} Server object
 */
async function start () {
  store = new Store()
  bot = new Bot()
  await bot.init(handleCommand)
  publicLobby = await bot.join(config.matrixLobby)
  // clean up from previous sessions (excluding parameter!)
  await bot.leaveAllRooms([publicLobby])
  server = new Server(store, bot)
  return server
}

/**
 * Callback function for Matrix which fires for every message
 *
 * @param {object} roomId Matrix room ID
 * @param {object} event Matrix event
 * @returns {boolean} success
 */
async function handleCommand (roomId, event) {
  // Don't handle events that don't have contents (they were probably redacted)
  if (!event.content) return

  // for rooms created by this bot for authentication
  if (authenticationRooms[roomId]) {
    console.log('Bot receptionist ignores authentication room: ', roomId)
    // if both invitees are now in the room #TK-60
    //   bot.leave(roomId)
    return
  }

  // The client sends `text` or `file`.
  if (event.content.msgtype === 'm.text') {
    const body = event.content.body

    // In the lobby, invite people who mention the lobby phrases into a private room
    if (roomId === publicLobby) {
      const invitees = [event.sender]
      let lobbyPhrase = body

      // when authenticating with another guest, the syntax is 'authenticate! @guest10101:matrix.org'
      if (body.startsWith('authenticate! ')) {
        lobbyPhrase = body.split(' ')[0]
        invitees.push(body.split(' ')[1])
      }

      // Invite everyone to this newly created room
      const room = await bot.createPrivateRoom(invitees, lobbyPhrase)

      if (lobbyPhrase === 'authenticate!') {
        authenticationRooms[room] = true
        console.log('Bot receptionist created authentication room:', room)
      }

      return
    }

    // Parse body as JSON
    let json
    try {
      json = JSON.parse(body)
    } catch (err) {
      json = undefined
    }

    // If valid JSON sent, treat it as a verifiable credential and try to verify it.
    if (json) {
      return server.verify(json, roomId, event)
    }

    // Make sure that the event looks like a command we're expecting
    const cli = body.split(' ')
    switch (cli[0]) {
      case '!verify':
        /* eslint-disable */
        return await server.handleVerificationFromUuid(server.cli[1])
        /* eslint-enable */
      case 'authenticate!':
      case 'verifyCredential!':
        console.error(`The command ${cli[0]} should already have been handled.`)
        return false
      default:
        console.log(`handleCommand: ignored ${body}`)
        bot.sendReplyNotice(roomId, event, 'Hello! I am a verifier bot.')
    }
  } else if (event.content.msgtype === 'm.file') {
    // gotta be verifiableCredential.zip
    if (event.content.filename !== 'verifiableCredential.zip') {
      return false
    }
    // The Matrix lite client which comes with matrix-bot-sdk doesn't have the API
    // so we have to use the MatrixClient API from matrix-js-sdk
    const mc = createClient(config.matrixServerURL)
    // Convert the 'mxc:' URL into a regular HTTPS url
    const url = mc.mxcUrlToHttp(event.content.url, undefined, undefined, undefined, true)
    // Fetch that zip
    const zipFile = await fetch(url)
    // Make it into a blob
    const blob = await zipFile.blob()
    // load the zip from an array buffer
    const zip = await JSZip.loadAsync(blob.arrayBuffer())
    // read credential.json from the zip file
    const credential = await zip.files['credential.json'].async('string')
    const credentialJson = JSON.parse(credential)
    // pass it on to the server's verify function
    return server.verify(credentialJson, roomId, event, zip)
  } // This daemon sends `notice`: Ignore our own messages.
}

module.exports = { start }

// Start server from command line with `node index verify start`
/* istanbul ignore if */
if ((process.argv[2] === 'verify') && (process.argv[3] === 'start')) {
  start()
}
