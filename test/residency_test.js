const residency = require('../src/residency.js')
const chai = require('chai')

// Configure chai
chai.should()
const expect = chai.expect

describe('Residency Unit Tests', function () {
  it('should verify a BCN Resident', async () => {
    const postalCode = '08003'
    expect(true, residency.isBcnResident(postalCode))
  })

  it('should verify a non-BCN Resident', async () => {
    const postalCode = '08999'
    expect(false, residency.isBcnResident(postalCode))
  })

  it('should verify an invalid Postal Code', async () => {
    const postalCode = 'abcd'
    expect(false, residency.validatePostalCode(postalCode))
  })
})
