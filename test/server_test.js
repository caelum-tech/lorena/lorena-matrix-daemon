const email = require('../src/email.js')
const { Bot } = require('../src/bot.js')
const { Server } = require('../src/server.js')
const { Store } = require('../src/store.js')
const chai = require('chai')
const chaiHttp = require('chai-http')
const chaiString = require('chai-string')
const Zen = require('@caelum-tech/zenroom-lib')

// Configure chai
chai.should()
chai.use(chaiHttp)
chai.use(chaiString)

describe('Server', function () {
  let server

  const residencyCredential = {
    '@context': ['https://www.w3.org/2018/credentials/v1', 'https://www.w3.org/2018/credentials/examples/v1'],
    claims: [{
      credentialStatus: {
        id: 'https://lorena.caelumlabs.com/credentials',
        type: 'CaelumResidencyCredential',
        currentStatus: 'pending',
        statusReason: 'self-issued'
      },
      credentialSubject: [{
        postalCode: '08003',
        type: 'CaelumResidencyCredential'
      }],
      id: 'did:lor:eb8677ace3a77d75a51a639ad6caefbc5aa60bf9bce32c6882ad9356464c3397',
      issuanceDate: '2019-11-07T13:36:32.511Z',
      issuer: 'did:lor:eb8677ace3a77d75a51a639ad6caefbc5aa60bf9bce32c6882ad9356464c3397',
      type: ['VerifiableCredential', 'CaelumResidencyCredential']
    }],
    id: 'did:lor:eb8677ace3a77d75a51a639ad6caefbc5aa60bf9bce32c6882ad9356464c3397',
    issuer: 'did:lrn:caelum-email-verifier:ebfeb1276e12ec21f712ebc6f1c#k2',
    nonRevocationProof: [{
      created: '2019-11-07T13:36:32.511Z',
      signatureValue: 'u64:BAmmvJDpwvTivxgl-zYWVlE0D_GIMqH7At7IM_hBj7e5vUIJzc15bucpwqxeotXOy6JUKPkX9yzkUURubYMbBxmzLJ_ZhXEXVH8INeVJ0oJAOeiaNKZ2tzcQF467lGOB54ClwNgHiYWWTy9xfkaC3EQ',
      type: 'CaelumEmailCredential',
      verificationMethod: 'Zenroom',
      zenroom: {
        draft: 'u64:eyJjcmVkZW50aWFsU3RhdHVzIjp7ImlkIjoiSGFzaDpfMHg4NjMzMzg1ODExZjA2ZTIyZDVmNjJlYjk1OGM4YjFhNzJhNmZiY2Y1OWM2ZjQ2NGY5ZDNiN2QwODYzNTVhYjNkIiwidHlwZSI6InBlbmRpbmcifSwidHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCIsIkNhZWx1bUVtYWlsQ3JlZGVudGlhbCJdLCJjcmVkZW50aWFsU3ViamVjdCI6W3sidHlwZSI6IkNhZWx1bUVtYWlsQ3JlZGVudGlhbCIsImVtYWlsIjoidG9tYXNAY2FlbHVtbGFicy5jb20iLCJmaXJzdCI6IlRvbWFzIiwibGFzdCI6IlRvbWFzIn1dLCJpZCI6Ikhhc2g6XzB4ZWI4Njc3YWNlM2E3N2Q3NWE1MWE2MzlhZDZjYWVmYmM1YWE2MGJmOWJjZTMyYzY4ODJhZDkzNTY0NjRjMzM5NyIsImlzc3VhbmNlRGF0ZSI6IjIwMTktMTEtMDdUMTM6MzY6MzIuNTExWiIsImlzc3VlciI6ImRpZDpscm46Y2FlbHVtLWVtYWlsLXZlcmlmaWVyOmViZmViMTI3NmUxMmVjMjFmNzEyZWJjNmYxYyNrMiJ9',
        signature: {
          r: 'u64:N02HC0QC4RXBXpw-721QoD1zu3-qdc6C4Mho4i7HXJd-Si8GqYJfO7lsFarvd7ZWjCdNSb6jRrs',
          s: 'u64:JyCGw3762wZX6pEbyMZVtDWrgSh4BWKhdMqltYx1eyWGcj89wmagXv6T3sRQWNRcpBcD7HS1TcI'
        }
      }
    }],
    proof: [],
    type: ['VerifiableCredential', 'CaelumResidencyCredential']
  }

  const emailCredential = {
    '@context': ['https://www.w3.org/2018/credentials/v1', 'https://www.w3.org/2018/credentials/examples/v1'],
    claims: [{
      credentialStatus: {
        id: 'https://lorena.caelumlabs.com/credentials',
        type: 'CaelumEmailCredential',
        currentStatus: 'pending',
        statusReason: 'self-issued'
      },
      credentialSubject: [{
        email: 'tomas@caelumlabs.com',
        first: 'Tomas',
        last: 'Tomas',
        type: 'CaelumEmailCredential'
      }],
      id: 'did:lor:eb8677ace3a77d75a51a639ad6caefbc5aa60bf9bce32c6882ad9356464c3397',
      issuanceDate: '2019-11-07T13:36:32.511Z',
      issuer: 'did:lor:eb8677ace3a77d75a51a639ad6caefbc5aa60bf9bce32c6882ad9356464c3397',
      type: ['VerifiableCredential', 'CaelumEmailCredential']
    }],
    id: 'did:lor:eb8677ace3a77d75a51a639ad6caefbc5aa60bf9bce32c6882ad9356464c3397',
    issuer: 'did:lrn:caelum-email-verifier:ebfeb1276e12ec21f712ebc6f1c#k2',
    nonRevocationProof: [{
      created: '2019-11-07T13:36:32.511Z',
      signatureValue: 'u64:BAmmvJDpwvTivxgl-zYWVlE0D_GIMqH7At7IM_hBj7e5vUIJzc15bucpwqxeotXOy6JUKPkX9yzkUURubYMbBxmzLJ_ZhXEXVH8INeVJ0oJAOeiaNKZ2tzcQF467lGOB54ClwNgHiYWWTy9xfkaC3EQ',
      type: 'CaelumEmailCredential',
      verificationMethod: 'Zenroom',
      zenroom: {
        draft: 'u64:eyJjcmVkZW50aWFsU3RhdHVzIjp7ImlkIjoiSGFzaDpfMHg4NjMzMzg1ODExZjA2ZTIyZDVmNjJlYjk1OGM4YjFhNzJhNmZiY2Y1OWM2ZjQ2NGY5ZDNiN2QwODYzNTVhYjNkIiwidHlwZSI6InBlbmRpbmcifSwidHlwZSI6WyJWZXJpZmlhYmxlQ3JlZGVudGlhbCIsIkNhZWx1bUVtYWlsQ3JlZGVudGlhbCJdLCJjcmVkZW50aWFsU3ViamVjdCI6W3sidHlwZSI6IkNhZWx1bUVtYWlsQ3JlZGVudGlhbCIsImVtYWlsIjoidG9tYXNAY2FlbHVtbGFicy5jb20iLCJmaXJzdCI6IlRvbWFzIiwibGFzdCI6IlRvbWFzIn1dLCJpZCI6Ikhhc2g6XzB4ZWI4Njc3YWNlM2E3N2Q3NWE1MWE2MzlhZDZjYWVmYmM1YWE2MGJmOWJjZTMyYzY4ODJhZDkzNTY0NjRjMzM5NyIsImlzc3VhbmNlRGF0ZSI6IjIwMTktMTEtMDdUMTM6MzY6MzIuNTExWiIsImlzc3VlciI6ImRpZDpscm46Y2FlbHVtLWVtYWlsLXZlcmlmaWVyOmViZmViMTI3NmUxMmVjMjFmNzEyZWJjNmYxYyNrMiJ9',
        signature: {
          r: 'u64:N02HC0QC4RXBXpw-721QoD1zu3-qdc6C4Mho4i7HXJd-Si8GqYJfO7lsFarvd7ZWjCdNSb6jRrs',
          s: 'u64:JyCGw3762wZX6pEbyMZVtDWrgSh4BWKhdMqltYx1eyWGcj89wmagXv6T3sRQWNRcpBcD7HS1TcI'
        }
      }
    }],
    proof: [],
    type: ['VerifiableCredential', 'CaelumEmailCredential']
  }

  before(async () => {
    const identity = 'did:something:else'
    const uuid = email.generateVerificationUuid()
    const url = email.generateVerificationURL(uuid)
    const text = email.generateEmailText(identity, url)
    const html = email.generateEmailHTML(identity, url)
    const store = new Store()
    store.add(uuid, { uuid, url, text, html })
    // var simple = require('simple-mock')
    var bot = new Bot(() => {})
    // simple.mock(bot, 'shutdown', 'value') // Replace with this value
    server = new Server(store, bot)
  })

  it('should check server health successfully', async () => {
    // server health
    await chai.request(server.app)
      .get('/system')
      .then((res) => {
        res.should.be.ok
        res.should.have.status(200)
      })
  })

  it('should handle known and unknown uuids through the API', async () => {
    // we're not testing the success case because that sends a notification,
    // and we're not testing that in this scenario.

    // an unknown uuid should return error
    await chai.request(server.app)
      .get(`/verify/${email.generateVerificationUuid()}`)
      .then((err) => {
        err.ok.should.be.false
        err.should.have.status(404)
      })
  })

  it('should return a signed credential from Email Claim', async () => {
    const z = new Zen()
    // should create keys correctly
    const alice0 = await z.newKeyPair('Alice')
    const alice = JSON.parse(JSON.stringify(alice0))
    const keypair = alice.Alice.keypair
    keypair.private_key.should.startWith('u64:')
    keypair.public_key.should.startWith('u64:')

    // we should have specified one proof above
    emailCredential.nonRevocationProof.length.should.eq(1)
    const credentialResult = await server.signFromCredential(keypair.private_key, keypair.public_key, emailCredential)
    // test to make sure a new proof gets added
    credentialResult.nonRevocationProof.length.should.eq(2)
    // test to make sure new proof is verifiable with Zenroom
    const verifiedCredential = await z.checkSignature(
      'Alice',
      {
        Alice: {
          public_key: credentialResult.nonRevocationProof[1].verificationMethod
        }
      },
      {
        zenroom: { curve: 'goldilocks', encoding: 'url64', version: '1.0.0+53387e8', scenario: 'simple' },
        Alice: credentialResult.nonRevocationProof[1].zenroom
      },
      'Bob'
    )
    verifiedCredential.signature.should.eq('correct')
  })

  it('should return a signed credential from Residency Claim', async () => {
    const z = new Zen()
    // should create keys correctly
    const alice0 = await z.newKeyPair('Alice')
    const alice = JSON.parse(JSON.stringify(alice0))
    const keypair = alice.Alice.keypair
    keypair.private_key.should.startWith('u64:')
    keypair.public_key.should.startWith('u64:')

    // we should have specified one proof above
    residencyCredential.nonRevocationProof.length.should.eq(1)
    const credentialResult = await server.signFromCredential(keypair.private_key, keypair.public_key, residencyCredential)
    // test to make sure a new proof gets added
    credentialResult.nonRevocationProof.length.should.eq(2)
    // test to make sure new proof is verifiable with Zenroom
    const verifiedCredential = await z.checkSignature(
      'Alice',
      {
        Alice: {
          public_key: credentialResult.nonRevocationProof[1].verificationMethod
        }
      },
      {
        zenroom: { curve: 'goldilocks', encoding: 'url64', version: '1.0.0+53387e8', scenario: 'simple' },
        Alice: credentialResult.nonRevocationProof[1].zenroom
      },
      'Bob'
    )
    verifiedCredential.signature.should.eq('correct')
  })

  after(async () => {
    await server.shutdown()
  })
})
