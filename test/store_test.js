const config = require('../src/config.js')
const email = require('../src/email.js')
const { Store } = require('../src/store.js')
const chai = require('chai')

// Configure chai
chai.should()
const expect = chai.expect

describe('Store', function () {
  it('should save the verification email to the store with the guid as key', async () => {
    const identity = 'did:something:else'
    const uuid = email.generateVerificationUuid()
    const url = email.generateVerificationURL(uuid)
    const text = email.generateEmailText(identity, url)
    const html = email.generateEmailHTML(identity, url)
    const store = new Store()
    store.add(uuid, { uuid, url, text, html })
    const retrieved = store.get(uuid)
    retrieved.uuid.should.eq(uuid)
    retrieved.url.should.eq(url)
    retrieved.text.should.eq(text)
    retrieved.html.should.eq(html)
    expect(store.remove(uuid)).to.be.true
    expect(store.remove(uuid)).to.be.false

    // adding an item already in the past means it should expire when I try to retrieve it.
    store.add(uuid, { uuid, url, text, html, timestamp: (new Date()).getTime() - config.emailVerificationTimeout })
    expect(store.get(uuid)).to.be.undefined
  })
})
