const config = require('../src/config.js')

describe('Email Verifier Daemon', function () {
  it('should have configuration information', async () => {
    config.matrixServerURL.should.not.to.be.empty
    config.matrixLobby.should.not.be.empty
    // config.matrixDaemonUserId.should.not.be.empty // optional
    // config.matrixDaemonPassword.should.not.be.empty // optional
    config.emailVerificationURLPrefix.should.eq('http://localhost:3000/')
    config.emailVerificationTimeout.should.be.gt(1000)
    config.emailTemplateSubject.should.not.to.be.empty
    config.emailTemplateText.should.not.to.be.empty
    config.emailTemplateHTML.should.not.to.be.empty
    config.emailSenderAddress.should.not.to.be.empty
  })
})
