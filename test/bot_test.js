const fetch = require('node-fetch')
const config = require('../src/config.js')
const { Bot } = require('../src/bot.js')
const helpers = require('@caelum-tech/lorena-matrix-helpers')

let bot, lobby
const signal = 'signalWord!'

/**
 * Test callback function for Matrix room
 *
 * @param {string} roomId Matrix room ID
 * @param {*} event Matrix event
 */
async function handleMessage (roomId, event) {
  // Don't handle events that don't have contents (they were probably redacted)
  if (!event.content) return

  // The client sends text.  The daemon sends notices. Ignore our own messages.
  if (event.content.msgtype !== 'm.text') return

  // If we're stalking a lobby to invite people who say the magic word into a private room,
  // and they say the magic word
  if ((roomId === lobby) && (event.content.body === signal)) {
    // invite the sender of this message to a new room.
    const room = await bot.createPrivateRoom([event.sender], signal)
    console.log('created room', room)
  }
}

describe('Bot', function () {
  it('should connect to the matrix server using the credentials for the verifier identity', async () => {
    bot = new Bot()
    await bot.init(handleMessage)
  })

  it('should join the lobby', async () => {
    lobby = await bot.join(config.matrixLobby)
  })

  it('should respond to a mention in the lobby with an invitation to a new room', async () => {
    // create a new guest
    const guest = await helpers.createMatrixGuest(config.matrixServerURL)

    // new guest joins lobby
    const guestLobby = await helpers.joinMatrixRoomAsGuest(config.matrixServerURL, config.matrixLobby, guest.access_token)
    guestLobby.room_id.should.eq(lobby)

    // new guest posts !signalWord in lobby
    const postSignal = await fetch(`${config.matrixServerURL}/_matrix/client/r0/rooms/${encodeURIComponent(guestLobby.room_id)}/send/m.room.message?access_token=${guest.access_token}`,
      { method: 'post', body: JSON.stringify({ msgtype: 'm.text', body: signal }), headers: { 'Content-Type': 'application/json' } })
    postSignal.ok.should.be.true

    const leave = await helpers.leaveMatrixRoom(config.matrixServerURL, guestLobby.room_id, guest.access_token)
    leave.ok.should.be.true

    // Check that the guest is now in the private room
    // Wow, this is hard because all of these things are asynchronous and extremely order-independent.
    // As a matter of fact, if you shut down the bot below right now, it won't even happen at all.
    // sleep(1000000000) ? Some weird callback? a callback for the callback callback callback?
  })

  it('should shut down the bot', async () => {
    await bot.leaveRoom(lobby)
    await bot.shutdown()
  })
})
