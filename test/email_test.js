const config = require('../src/config.js')
const email = require('../src/email.js')
const chai = require('chai')

// Configure chai
chai.should()
const expect = chai.expect

describe('Email', function () {
  it('should verify an email address', async () => {
    email.isEmailAddressValid('ffs').should.be.false
    email.isEmailAddressValid('a@b.c').should.be.false
    email.isEmailAddressValid('a+b@c.de').should.be.true
    email.isEmailAddressValid('a.b.c@e.fg').should.be.true
    email.isEmailAddressValid('ABC@DEFG.HIJK').should.be.true
  })

  it('should generate a verification UUID', async () => {
    const uuid1 = email.generateVerificationUuid()
    uuid1.should.not.be.empty
    const uuid2 = email.generateVerificationUuid()
    uuid2.should.not.eq(uuid1)
  })

  it('should generate a verification URL', async () => {
    const uuid = email.generateVerificationUuid()
    const url = email.generateVerificationURL(uuid)
    url.should.include(uuid)
    url.should.include(config.emailVerificationURLPrefix)
  })

  it('should generate a verification email', async () => {
    const uuid = email.generateVerificationUuid()
    const url = email.generateVerificationURL(uuid)
    expect(email.generateEmailHTML('BogIdent', url)).to.include(url)
  })

  it('should send the verification email and save message id', async () => {
    const identity = 'did:what:ever'
    const to = 'verification+test@caelumlabs.com'
    const msg = await email.send(identity, to)
    msg.identity.should.eq('did:what:ever')
    msg.messageid.should.not.be.empty // https://sendgrid.com/docs/glossary/x-message-id/
    msg.uuid.should.not.be.empty
    msg.to.should.eq(to)
    msg.from.should.eq(config.emailSenderAddress)
  })
})
