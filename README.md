# Lorena Matrix Daemon
NodeJS server daemon to carry out room management and verifications for DIDs.

|Branch|Pipeline|Coverage|
|:-:|:-:|:-:|
|[`master`](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon/tree/master)|[![pipeline status](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon/badges/master/pipeline.svg)](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon/commits/master)|[![coverage report](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon/badges/master/coverage.svg)](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-daemon/commits/master)|

## Flow

* The daemon watches for new room invitations on the verifier identity.
* When the verifier identity receives an invitation, the daemon joins the room.
* When an email address is posted in a room the daemon sends a verification email to that address.
* If an email bounces the daemon posts an error message to that room.
* When the recipient clicks the link to verify the email address the daemon posts the Credential and signature to the room.
* The daemon leaves the room. (Currently unimplemented)

## Run tests

Install dependencies
```bash
npm i
```

Run the tests
```bash
npm test
```

## Run daemon

Install dependencies
```bash
npm i
```

Set environment variables (documented in `.env`).
```bash
node index verify start
```
Expose port 3000 to the open Internet accessible by the address `CEVD_EMAIL_VERIFICATION_URL_PREFIX`.

## See also
[Lorena Matrix Client](https://gitlab.com/caelum-tech/Lorena/lorena-matrix-client)
