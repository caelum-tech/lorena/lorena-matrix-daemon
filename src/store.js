const config = require('../src/config.js')

/**
 * A simple storage class to keep messages in memory while the verification activities are carried out.
 */
class Store {
  constructor () {
    this.map = new Map()
  }

  /**
   * Add a message by key
   *
   * @param {*} key Identifier
   * @param {*} value message
   */
  add (key, value) {
    if (!value.timestamp) {
      value.timestamp = (new Date()).getTime()
    }
    this.map.set(key, value)
  }

  /**
   * Get a message by key. If the message has expired, remove it from the map and don't return it.
   *
   * @param {*} key identifier
   * @returns {*} value
   */
  get (key) {
    var item = this.map.get(key)
    if (!item) {
      return undefined
    }

    const now = (new Date()).getTime()
    const timeout = item.timestamp + config.emailVerificationTimeout

    if (timeout > now) {
      return item
    } else {
      this.remove(key)
      return undefined
    }
  }

  /**
   * Remove message by key
   *
   * @param {*} key identifier
   * @returns {boolean} success
   */
  remove (key) {
    return this.map.delete(key)
  }
}

module.exports = { Store }
