const express = require('express')
const email = require('./email.js')
const residency = require('./residency.js')
const http = require('http')
const Zen = require('@caelum-tech/zenroom-lib')

/**
 * Implements responses to requests for verification from Matrix
 */
class Server {
  constructor (store, bot) {
    this.store = store
    this.bot = bot
    this.app = express()
    const port = process.env.PORT || '3000'
    this.app.set('port', port)

    this.server = http.createServer(this.app)

    // Add server as middleware
    this.app.use((req, _res, next) => {
      req.server = this
      next()
    })

    this.app.get('/system', function (req, res) {
      res.sendStatus(200)
    })

    this.app.get('/verify/:uuid', async function (req, res) {
      const hv = await req.server.handleVerificationFromUuid(req.params.uuid)
      res.sendStatus(hv ? 200 : 404)
    })

    this.server.debug = true
    this.server.listen(port)
    this.server.on('error', onError)
    this.server.on('listening', onListening)
  }

  /**
   * Shut down server
   */
  async shutdown () {
    await this.bot.shutdown()
    await this.server.close()
    this.server = undefined
    this.app = undefined
  }

  /**
   * Verify the supplied credential with the appropriate method
   *
   * @param {object} credential VC: supported Verification types are CaelumEmailCredential and CaelumResidencyCredential
   * @param {string} roomId Matrix room
   * @param {*} event Matrix event
   * @param {*} files optional Zip archive of files
   * @returns {boolean} result
   */
  async verify (credential, roomId, event, files = undefined) {
    const claim = credential.claims[0]
    if (!claim) {
      console.error('Server.verify: claim not found', credential)
      return false
    }
    const credentialSubject = claim.credentialSubject[0]
    if (!credentialSubject) {
      console.error('Server.verify: credentialSubject not found', claim)
      return false
    }
    const credentialType = claim.credentialSubject[0].type
    if (credentialType === 'CaelumEmailCredential') {
      return this.verifyEmail(credential, roomId, event)
    } else if (credentialType === 'CaelumResidencyCredential') {
      return this.verifyBcnResidency(credential, roomId, event, files)
    } else {
      console.error('Server.verify: Invalid credentialType', credentialType)
      return false
    }
  }

  /**
   * Verify residency
   *
   * @param {object} credential VC to verify
   * @param {string} roomId Matrix room
   * @param {*} event Matrix event
   * @param {*} files Files in a Zip
   * @returns {boolean} result
   */
  async verifyBcnResidency (credential, roomId, event, files) {
    const postalCode = credential.claims[0].credentialSubject[0].postalCode

    if (residency.validatePostalCode(postalCode) === false) {
      this.bot.sendReplyNotice(roomId, event, 'Invalid postal code!')
      return false
    }

    if (residency.isBcnResident(postalCode) === false) {
      this.bot.sendReplyNotice(roomId, event, 'Not a Barcelona resident!')
      return false
    }

    // add these attributes and save the info to the store
    this.bot.sendReplyNotice(roomId, event, 'Progress: Barcelona resident!')
    return this.handleVerification({ credential, event, roomId })
  }

  /**
   * Verify email
   *
   * @param {object} credential VC to verify
   * @param {string} roomId Matrix room
   * @param {*} event Matrix event
   * @returns {boolean} result
   */
  async verifyEmail (credential, roomId, event) {
    try {
      const identity = credential.id
      const address = credential.claims[0].credentialSubject[0].email

      if (!email.isEmailAddressValid(address)) {
        this.bot.sendReplyNotice(roomId, event, `Email address "${address}" invalid`)
        return false
      }

      // send the verification email
      var msg = await email.send(identity, address)

      // add these attributes and save the info to the store
      msg = { ...msg, credential, event, roomId }
      this.store.add(msg.uuid, msg)

      // send a reply telling them to check their email
      this.bot.sendReplyNotice(roomId, event, `Progress: Email sent to "${address}", click on the link inside to verify.`)
      return true
    } catch (err) {
      this.bot.sendReplyNotice(roomId, event, `Error verifying credential: ${err}`)
      return false
    }
  }

  /**
   * Look up message stored by UUID
   * Used for asynchronous processing of email verifications
   *
   * @param {string} uuid request identifier
   * @returns {object} message
   */
  getMsgFromUuid (uuid) {
    const msg = this.store.get(uuid)
    if (!msg) {
      console.log(`Server.getMsgFromUuid: unknown uuid "${uuid}"`)
      return undefined
    } else {
      return msg
    }
  }

  /**
   * Handle the message associated with this UUID
   * This is used when processing an email verification because it is VERY asynchronous
   *
   * @param {string} uuid request identifier
   * @returns {boolean} result
   */
  async handleVerificationFromUuid (uuid) {
    const msg = this.getMsgFromUuid(uuid)
    if (msg && (await this.handleVerification(msg))) {
      this.store.remove(uuid)
      return true
    }
    return false
  }

  /**
   *  Use the message provided to supply a proof of the credential
   *
   * @param {object} msg message to verify
   * @returns {boolean} result
   */
  async handleVerification (msg) {
    const z = new Zen()
    const newCredential = msg.credential
    // Verify self-signed credential
    const selfSigned = await z.checkSignature(
      'Alice',
      {
        Alice: {
          public_key: newCredential.nonRevocationProof[0].verificationMethod
        }
      },
      {
        zenroom: { curve: 'goldilocks', encoding: 'url64', version: '1.0.0+53387e8', scenario: 'simple' },
        Alice: newCredential.nonRevocationProof[0].zenroom
      },
      'Bob'
    )
    if (selfSigned.signature !== 'correct') {
      console.error('Message verification: Credential not valid')
      throw new Error('Message verification: Credential not valid')
    }

    // var credential = vcdm.VCredential.fromJSON(msg.credential)
    // var credential = JSON.parse(msg.credential)
    const aliceZ = await z.newKeyPair('Alice')
    const signedCredential = await this.signFromCredential(
      aliceZ.Alice.keypair.private_key,
      aliceZ.Alice.keypair.public_key,
      this.createCredential(msg.credential)
    )

    // post signed credential to room
    this.bot.sendReplyNotice(msg.roomId, msg.event, JSON.stringify(signedCredential))
    return true
  }

  /**
   * Add a new accepted claim to the credential
   *
   * @param {object} credential original credential
   * @returns {object} credential with additional claim
   */
  createCredential (credential) {
    var claim1 = JSON.parse(JSON.stringify(credential.claims[0]))
    claim1.credentialStatus.currentStatus = 'accepted'
    claim1.credentialStatus.statusReason = 'Email receipt verified'
    credential.claims.push(claim1)
    return credential
  }

  /**
   * Sign the credential
   *
   * @param {object} privateKey private key
   * @param {object} publicKey public key
   * @param {object} cred credential in
   * @returns {object} credential out
   */
  async signFromCredential (privateKey, publicKey, cred) {
    var credential = JSON.parse(JSON.stringify(cred))
    const z = new Zen()
    const signedCredential = await z.signMessage(
      'Alice',
      {
        Alice: {
          keypair: { private_key: privateKey, public_key: publicKey }
        }
      },
      JSON.stringify(credential.claims[credential.claims.length - 1])
    )
    const zproofJson = {
      type: credential.claims[0].credentialSubject[0].type,
      created: new Date(Date.now()).toISOString(),
      verificationMethod: publicKey,
      signatureValue: publicKey,
      zenroom: signedCredential.Alice
    }
    var newProof = credential.nonRevocationProof
    newProof.push(zproofJson)
    credential.nonRevocationProof = newProof
    return credential
  }
}

/**
 * http server method
 */
function onListening () {
  var addr = this.address()
  /* istanbul ignore next */
  var bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port
  /* istanbul ignore else */
  if (this.debug) {
    console.log('Listening on ' + bind)
  }
}

/* istanbul ignore next */
/**
 * http server method
 *
 * @param {*} error to throw
 */
function onError (error) {
  if (error.syscall !== 'listen') {
    throw error
  }
}

module.exports = { Server }
/* eslint-enable */
