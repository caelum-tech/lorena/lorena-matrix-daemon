/**
 * Is this postal code in Barcelona?
 *
 * @param {string} postalCode Postal code to test
 * @returns {boolean} result
 */
function isBcnResident (postalCode) {
  return (postalCode >= '08001' && postalCode <= '08960')
}

/**
 * Is this a valid postal code?
 *
 * @param {string} postalCode to check
 * @returns {boolean} result
 */
function validatePostalCode (postalCode) {
// derived from https://emailregex.com/
  const postalCodePattern = /^\d{5}$/
  if (postalCodePattern.test(postalCode)) {
    return true
  } else {
    console.log('Invalid postal code!', postalCode)
    return false
  }
}

module.exports = { isBcnResident, validatePostalCode }
