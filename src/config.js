require('dotenv').config()

class Config {
  constructor () {
    this.matrixServerURL = process.env.CEVD_MATRIX_SERVER_URL
    this.matrixLobby = process.env.CEVD_MATRIX_LOBBY
    this.matrixDaemonAccessToken = process.env.CEVD_MATRIX_DAEMON_ACCESS_TOKEN
    this.matrixDaemonUserId = process.env.CEVD_MATRIX_DAEMON_USER_ID
    this.matrixDaemonPassword = process.env.CEVD_MATRIX_DAEMON_PASSWORD
    this.emailVerificationURLPrefix = process.env.CEVD_EMAIL_VERIFICATION_URL_PREFIX
    this.emailVerificationTimeout = Number(process.env.CEVD_EMAIL_VERIFICATION_TIMEOUT)
    this.emailTemplateSubject = process.env.CEVD_EMAIL_TEMPLATE_SUBJECT
    this.emailTemplateText = process.env.CEVD_EMAIL_TEMPLATE_TEXT
    this.emailTemplateHTML = process.env.CEVD_EMAIL_TEMPLATE_HTML
    this.emailSenderAddress = process.env.CEVD_EMAIL_SENDER_ADDRESS
  }
}

module.exports = new Config()
