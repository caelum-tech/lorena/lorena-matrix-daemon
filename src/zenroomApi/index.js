/* eslint-disable */
const zenroom = require('zenroom').default


/* * * * * * * * * * * * * * * * * * * * * *
 *           SIGNING SCHEME
 *  * * * * * * * * * * * * * * * * * * * */

 /**
  * Creates a private key and a public key using zencode.
  *
  * Output example:
  * {
  *     "public_key":"u64:BCYfOkN2YUXJxGYToRt3Z1ESg_niBQ1DZbwI8lZnX04KmR4tYjL5zy40u5FZvtoi93GWvIQYalusL4KsgfOt73UrO2vixOztl2pzkhBh6HVBWiLRTXtCb_HaIZFGyqxMF_hMI30ZWzsSgKgiZsb-YQw",
  *     "private_key":"u64:Ok51-tVCCCFCC4FOj6SgL0KPjHxELqDN8aEtODCo0LDcIHxy5T8k2Ns79zn2n23a_qszfGE5g5A"
  * }
  */
const zenroomCreateKeypair = () => {
  return new Promise((resolve, reject) => {
    const script = `
    Rule check version 1.0.0
    Scenario 'simple': Create the keypair
    Given that I am known as 'Alice'
    When I create the keypair
    Then print my data
    `

    const options = {
      script: script,
      verbosity: 1,
      print: (data) => {
        const response = JSON.parse(data)
        resolve(response.Alice.keypair)
      },
      success: () => {
        console.log('zenroomCreateKeypair: everything went smoothly')
      },
      error: () => {
        console.error('zenroomCreateKeypair: something very bad happened')
        reject()
      }
    }

    zenroom
      .init(options)
      .zencode_exec()
  })
}

/**
 *
 * @param {*} secretKey
 * @param {*} publicKey
 * @param {*} message
 *
 * Example input for `zenroomSignMessage`:
 *  secretKey = "u64:Ok51-tVCCCFCC4FOj6SgL0KPjHxELqDN8aEtODCo0LDcIHxy5T8k2Ns79zn2n23a_qszfGE5g5A"
 *  publicKey = "u64:BCYfOkN2YUXJxGYToRt3Z1ESg_niBQ1DZbwI8lZnX04KmR4tYjL5zy40u5FZvtoi93GWvIQYalusL4KsgfOt73UrO2vixOztl2pzkhBh6HVBWiLRTXtCb_HaIZFGyqxMF_hMI30ZWzsSgKgiZsb-YQw"
 *  signature = "Any msg of type string"
 *
 *
 * Output Example:
 * {
 *      "draft": "u64:VGhpc19pc19teV9zaWduZWRfbWVzc2FnZV90b19Cb2Iu",
 *      "signature": {
 *          "s": "u64:JyDXOTi-TK4Kj8OmjxxGe0d94jzC6_Btsc1eOtaK62RzyyYEUTBQsqf8BmZPCnDwo4-oIQDwNjQ",
 *          "r": "u64:NbgThS8jwsVcNDyMrVHAoIkmcCdde_IMjQW-hwexQImfINt7fCu6amX4JQt2iTkZkrBIXLLuv_E"
 *      }
 * }
 *
 * ZENCODE-OUTPUT:
 * {
 *     "zenroom": {
 *         "curve": "goldilocks",
 *         "encoding": "url64",
 *         "version": "1.0.0+4f64c96",
 *         "scenario": "simple"
 *     },
 *     "Alice": {
 *         "draft": "u64:VGhpc19pc19teV9zaWduZWRfbWVzc2FnZV90b19Cb2Iu",
 *         "signature": {
 *             "s": "u64:JyDXOTi-TK4Kj8OmjxxGe0d94jzC6_Btsc1eOtaK62RzyyYEUTBQsqf8BmZPCnDwo4-oIQDwNjQ",
 *             "r": "u64:NbgThS8jwsVcNDyMrVHAoIkmcCdde_IMjQW-hwexQImfINt7fCu6amX4JQt2iTkZkrBIXLLuv_E"
 *         }
 *     }
 * }
 *
 */
const zenroomSignMessage = (secretKey, publicKey, message) => {
  const script = `
    Rule check version 1.0.0
    Scenario 'simple': Alice signs a message for Bob
    Given that I am known as 'Alice'
    and I have my valid 'keypair'
    When I write '${message}' in 'draft'
    and I create the signature of 'draft'
    Then print my 'signature'
    and print my 'draft'
  `
  const keys = {
    "zenroom": {
        "curve": "goldilocks",
        "encoding" :"url64",
        "version" :"1.0.0+53387e8",
        "scenario" :"simple"
    },
    "Alice": {
        "keypair": {
            "public_key": publicKey,
            "private_key": secretKey
        }
    }
  }

  var response;
  const options = {
    script,
    keys,
    verbosity: 1,
    print: (data) => { response = JSON.parse(data) },
    success: () => { console.log('zenroomSignMessage: everything went smoothly') },
    error: () => { console.error('zenroomSignMessage: something very bad happened') }
  }

  zenroom
    .init(options)
    .zencode_exec().reset();

  return response['Alice']
}

/**
 *
 * @param {*} publicKey
 * @param {*} draft
 * @param {*} signature
 *
 * Example input for `zenroomVerifyMessage`
 *
 *  publicKey = "u64:BCYfOkN2YUXJxGYToRt3Z1ESg_niBQ1DZbwI8lZnX04KmR4tYjL5zy40u5FZvtoi93GWvIQYalusL4KsgfOt73UrO2vixOztl2pzkhBh6HVBWiLRTXtCb_HaIZFGyqxMF_hMI30ZWzsSgKgiZsb-YQw"
 *  draft = "u64:VGhpc19pc19teV9zaWduZWRfbWVzc2FnZV90b19Cb2Iu"
 *  signature = {
 *      "s": "u64:JyDXOTi-TK4Kj8OmjxxGe0d94jzC6_Btsc1eOtaK62RzyyYEUTBQsqf8BmZPCnDwo4-oIQDwNjQ",
 *      "r": "u64:NbgThS8jwsVcNDyMrVHAoIkmcCdde_IMjQW-hwexQImfINt7fCu6amX4JQt2iTkZkrBIXLLuv_E"
 *  }
 *
 * Output Example:
 *  {
 *      msg: "Original message or empty if error",
 *      verification: "'correct'  or 'incorrect' if error"
 *  }
 *
 * ZENCODE-OUTPUT
 *   {
 *       "zenroom": {
 *           "curve": "goldilocks",
 *           "encoding": "url64",
 *           "version": "1.0.0+4f64c96",
 *           "scenario": "simple"
 *       },
 *       "draft": "This_is_my_signed_message_to_Bob.",
 *       "signature": "correct"
 *   }
 *
 */
const zenroomVerifyMessage = (publicKey, draft, signature) => {
  const script = `
    rule check version 1.0.0
    Scenario 'simple': Bob verifies the signature from Alice
    Given that I am known as 'Bob'
    and I have a valid 'public key' from 'Alice'
    and I have a valid 'signature' from 'Alice'
    and I have a 'draft'
    When I verify the 'draft' is signed by 'Alice'
    Then print 'signature' 'correct' as 'string'
    and print as 'string' the 'draft'
  `
  const keys = {
    "zenroom": {"curve":"goldilocks","encoding":"url64","version":"1.0.0+53387e8","scenario":"simple"},
    "Alice": {
      "public_key": publicKey,
      "draft": draft,
      "signature": signature
    }
  }

  var response;
  const options = {
    script,
    keys,
    //data,
    verbosity: 1,
    print: (d) => { response = JSON.parse(d) },
    success: () => { console.log('zenroomVerifyMessage: everything went smoothly') },
    error: () => { console.error('zenroomVerifyMessage: something very bad happened') }
  }

  zenroom
    .init(options)
    .zencode_exec().reset();

  if( response.hasOwnProperty('signature') && response['signature'] === "correct"){
    return {
      msg: response['draft'],
      verification: "correct"
    }
  } else {
    return {
      msg: "",
      verification: "incorrect"
    }
  }
}


/* * * * * * * * * * * * * * * * * * * * * *
 *          CREDENTIAL SCHEME
 *  * * * * * * * * * * * * * * * * * * * */

/*
    Step 1.
    Issuer generates an issuer keypair
    ( nothing -> issuer_keypair)

    Scenario coconut: issuer keygen
    Given that I am known as 'Issuer'
    When I create the issuer keypair
    Then print my 'issuer keypair'

*/

/*
    Step 1a.
    Issuer publishes the verification key
    ( issuer_keypair -> issuer_verifier)

    Scenario coconut: publish verifier
    Given that I am known as 'Issuer'
    and I have my valid 'verifier'
    Then print my 'verifier'

*/

/*
    Step 2.
    Holder generates her credential keypair
    ( nothing -> credential_keypair)

    Scenario coconut: credential keygen
    Given that I am known as 'Holder'
    When I create the credential keypair
    Then print my 'credential keypair'

*/

/*
    Step 3.
    Holder sends her credential signature request
    ( credential_keypair -> credential_request)

    Scenario coconut: create request
    Given that I am known as 'Holder'
    and I have my valid 'credential keypair'
    When I create the credential request
    Then print my 'credential request'

*/

/*
    Step 4.
    Issuer decides to sign a credential signature request
    ((credential_request, issuer_keypair) -> issuer_signature)

    Scenario coconut: issuer sign
    Given that I am known as 'Issuer'
    and I have my valid 'issuer keypair'
    and I have a valid 'credential request'
    When I create the credential signature
    Then print the 'credential signature'
    and print the 'verifier'

*/

/*
    Step 5.
    Holder receives and aggregates the signed credential
    ( (issuer_signature, credential_keypair) -> credential)

    Scenario coconut: aggregate signature
    Given that I am known as 'Holder'
    and I have my valid 'credential keypair'
    and I have a valid 'credential signature'
    When I create the credentials
    Then print my 'credentials'
    and print my 'credential keypair'

*/

/*
    Step 6.
    Create and publish a blind credential proof (to Substrate)
    ((credentials, credential_keypair, verifier(by issuer)) -> credential_proof)

    Scenario coconut: create proof
    Given that I am known as 'Holder'
    and I have my valid 'credential keypair'
    and I have a valid 'verifier' from 'Issuer'
    and I have my valid 'credentials'
    When I aggregate the verifiers
    and I create the credential proof
    Then print the 'credential proof'
*/

/*
    Step 7.
    Anyone can verify the proof (Verifier on Substrate - Maybe Proof should be inside the credential)
    ((credential_proof, verifier) -> OK )

    Scenario coconut: verify proof
    Given that I have a valid 'verifier' from 'Issuer'
    and I have a valid 'credential proof'
    When I aggregate the verifiers
    and I verify the credential proof
    Then print 'Success' 'OK' as 'string'
*/

module.exports = {
  zenroomCreateKeypair,
  zenroomSignMessage,
  zenroomVerifyMessage
}
/* eslint-enable */
