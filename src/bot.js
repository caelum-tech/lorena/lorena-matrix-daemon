const { MatrixClient, SimpleFsStorageProvider, AutojoinRoomsMixin } = require('matrix-bot-sdk')
const config = require('../src/config.js')
const { loginMatrixUser } = require('@caelum-tech/lorena-matrix-helpers')

/**
 * Implements a Matrix bot using the Matrix Bot SDK
 */
class Bot {
  /**
   * Create a new private room and invite the specified users
   *
   * @param {Function} callback - room message handler
   * @returns {Promise} when started
   */
  async init (callback) {
    const storage = new SimpleFsStorageProvider('bot.json')
    let accessToken = config.matrixDaemonAccessToken

    // Work with the access token if provided, otherwise log in
    if (!accessToken) {
      const session = await loginMatrixUser(config.matrixServerURL, config.matrixDaemonUserId, config.matrixDaemonPassword)
      accessToken = session.access_token
      // Failure to log in. This isn't a use case we test.
      /* istanbul ignore if */
      if (!accessToken) {
        throw session.error
      }
    }

    this.client = new MatrixClient(config.matrixServerURL, accessToken, storage)

    AutojoinRoomsMixin.setupOnClient(this.client)
    this.client.userId = config.matrixDaemonUserId

    return new Promise((resolve, reject) => {
      this.client.start().then(() => {
        console.log('Matrix client started!')
        this.client.on('room.message', callback)
        resolve()
      })
    })
  }

  /**
   * Create a new private room and invite the specified users
   *
   * @param {string[]} invitees - Matrix user IDs to invite to the new room
   * @param {string} topic - Matrix user IDs to invite to the new room
   * @returns {*} room info
   */
  async createPrivateRoom (invitees, topic) {
    /* istanbul ignore if */
    if (!this.client) {
      console.error('Bot.createPrivateRoom failure, already shutting down')
      return
    }

    // invite the sender of this message to a new room.
    return this.client.createRoom({
      preset: 'private_chat', // template
      visibility: 'private', // Either 'public' or 'private'.
      invite: invitees, // A list of user IDs to invite to this room.
      topic // often the signal which is the reason for the room
    })
  }

  /**
   * Leave all rooms currently joined
   *
   * @param {string[]} except - Matrix room IDs *not* to leave (in the format !random:server.name)
   */
  async leaveAllRooms (except = []) {
    // clean up old rooms hanging around from previous sessions
    const rooms = await this.client.getJoinedRooms()
    for (const roomId of rooms) {
      // don't leave the room(s) specified
      if (except.includes(roomId)) {
        continue
      }

      // race condition on shutdown
      /* istanbul ignore else */
      if (!this.client) {
        console.error('Bot.leaveAllRooms: client disappeared while leaving room due to race condition on shutdown.')
        return
      }

      try {
        await this.leaveRoom(roomId)
      } catch (err) {
        console.error(err)
      }
    }
  }

  /**
   * Leave a Matrix room. If throttled, wait and try again.  RECURSION
   *
   * @param {string} roomId room to leave
   */
  async leaveRoom (roomId) {
    try {
      await this.client.leaveRoom(roomId)
    } catch (err) {
      if (err.body.errcode === 'M_LIMIT_EXCEEDED') {
        console.log(`Bot.leaveRoom failed to leave ${roomId}, waiting ${err.body.retry_after_ms}`)
        await this.sleep(err.body.retry_after_ms)
        await this.leaveRoom(roomId)
      } else {
        throw err
      }
    }
  }

  /**
   * Sleep for the specified length of time
   *
   * @param {number} milliseconds time to sleep
   * @returns {Promise} the promise you should await
   */
  sleep (milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds))
  }

  /**
   * send a notice message in the form of a reply
   * Don't use RichReply because we want it to be a message,
   * and we don't want to quote the original message in the text.
   *
   * @param {string} roomId - Room ID (in the form !random:server.name)
   * @param {Event} event - Event to which we are responding
   * @param {string} message - Text to include
   */
  sendReplyNotice (roomId, event, message) {
    const notice = {
      'm.relates_to': {
        'm.in_reply_to': {
          event_id: event.event_id
        }
      },
      msgtype: 'm.notice',
      body: message
    }

    this.client.sendMessage(roomId, notice)
  }

  /**
   * Join the specified room
   *
   * @param {string} roomIdOrAlias - Room ID in the format !random:server.name or #alias:server.name
   * @returns {string} roomId
   */
  async join (roomIdOrAlias) {
    return this.client.joinRoom(roomIdOrAlias)
  }

  /**
   * Shut down the bot
   */
  async shutdown () {
    // Bot is sometimes used in testing and not initialized
    if (this.client) {
      await this.client.stop()
      this.client = undefined
    }
  }
}

module.exports = { Bot }
