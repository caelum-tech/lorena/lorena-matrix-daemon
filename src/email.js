const config = require('../src/config.js')
const uuidv4 = require('uuid/v4')
const nodemailer = require('nodemailer')

/**
 * Is a valid email address
 *
 * @param {string} address email address
 * @returns {boolean} result
 */
function isEmailAddressValid (address) {
  // derived from https://emailregex.com/
  const regex =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return regex.test(address)
}

/**
 * Generate email text to send
 *
 * @param {string} identity identity referenced
 * @param {string} verificationLink URL
 * @returns {string} text
 */
function generateEmailText (identity, verificationLink) {
  return generateEmail(config.emailTemplateText, identity, verificationLink)
}

/**
 * Generate email HTML to send
 *
 * @param {string} identity identity referenced
 * @param {string} verificationLink URL
 * @returns {string} HTML
 */
function generateEmailHTML (identity, verificationLink) {
  return generateEmail(config.emailTemplateHTML, identity, verificationLink)
}

/**
 * Generate email to send
 *
 * @param {string} template string to be replaced
 * @param {string} identity identity referenced
 * @param {string} verificationLink URL
 * @returns {string} HTML or text
 */
function generateEmail (template, identity, verificationLink) {
  return (eval('`' + template + '`')) // eslint-disable-line no-eval
}

/**
 * generate the unique identifier for the request being handled
 *
 * @returns {string} uuid
 */
function generateVerificationUuid () {
  return uuidv4()
}

/**
 * generate the verification URL
 *
 * @param {string} uuid identifier for request
 * @returns {string} URL
 */
function generateVerificationURL (uuid) {
  return config.emailVerificationURLPrefix + 'verify/' + uuid
}

/**
 * Send email
 *
 * @param {string} identity recipient
 * @param {string} to email address
 * @returns {*} message info
 */
async function send (identity, to) {
  let testAccount

  // If we're running a test, create a mail test account and use it
  const test = typeof global.it === 'function'
  /* istanbul ignore else */
  if (test) {
    // all emails are caught by ethereal.email
    testAccount = await nodemailer.createTestAccount()
    console.log('send test mode')
  }

  const host = test ? 'smtp.ethereal.email' : /* istanbul ignore next */ process.env.CEVD_EMAIL_HOST
  const port = test ? 587 : /* istanbul ignore next */ process.env.CEVD_EMAIL_HOST
  const user = test ? testAccount.user : /* istanbul ignore next */ process.env.CEVD_EMAIL_AUTH_USER
  const pass = test ? testAccount.pass : /* istanbul ignore next */ process.env.CEVD_EMAIL_AUTH_PASS
  const uuid = generateVerificationUuid()
  const url = generateVerificationURL(uuid)
  const transporter = nodemailer.createTransport({ host, port, auth: { user, pass } })
  const msg = {
    to,
    from: config.emailSenderAddress,
    subject: config.emailTemplateSubject,
    text: generateEmailText(identity, url),
    html: generateEmailHTML(identity, url)
  }
  const result = await transporter.sendMail(msg)
  msg.messageid = result.messageId
  msg.identity = identity
  msg.uuid = uuid
  return msg
}

module.exports = { isEmailAddressValid, generateEmailText, generateEmailHTML, generateVerificationUuid, generateVerificationURL, send }
