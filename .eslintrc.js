module.exports = {
    'env': {
      'browser': true,
      'commonjs': true,
      'es6': true,
      'mocha': true
    },
    'extends': [
      'standard',
      'plugin:jsdoc/recommended'
    ],
    'globals': {
      'artifacts': false,
      'Atomics': 'readonly',
      'contract': false,
      'SharedArrayBuffer': 'readonly',
      'web3': false
    },
    'parserOptions': {
      'ecmaVersion': 2018
    },
    'rules': {
      'no-unused-expressions': 0,
      'chai-friendly/no-unused-expressions': 2
    },
    'plugins': [
      'chai-friendly',
      'jsdoc'
    ]
  }
