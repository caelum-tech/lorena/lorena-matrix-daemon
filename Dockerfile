# Caelum LABS Official Image <albert@caelumlabs.com>
# This image contains nodejs and caelum-verifier-daemon

# specify the node base image with your desired version node:<version>
# https://nodejs.org/es/about/releases/
#FROM node:12.4-alpine
FROM node:10-jessie
LABEL maintainer="Albert Valverde <albert@caelumlabs.com>"
LABEL description="Caelum Worker Docker Image"

ENV PORT="3004"
EXPOSE 3004

# Installing tools
RUN apt-get update
RUN apt-get install -y git bash openssh-client curl grep python vim nmon net-tools
RUN apt-get install -y build-essential lsb-release curl  apt-transport-https

# Copying App repository
COPY . /home/node/app
WORKDIR /home/node/app

RUN npm i

# DEBUG MODE
# ENV DEBUG=true

CMD [ "node", "index", "verify", "start" ]
